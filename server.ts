import express = require('express');

const app: express.Application = express();

interface IPerson {
  name: string;
  age: number;
  city?: string;
}

interface IStudent extends IPerson {
  school: string;
}

class Person  {
  public name: string
  public age: number

  constructor(name: string, age: number) {
    this.name = name
    this.age = age
  }

  public showMe() {
    console.log(`Hello, I am ${this.getName()}, and I am ${this.age} years old`)
  }

  private getName() {
    return this.name
  }
}

// middlewares

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

function msgMiddleware(req: express.Request, res: express.Response, next: () => void) {
  const person = new Person('Rodrigo', 20)
  person.showMe()

  next();
}

app.use(msgMiddleware);


// rotas

app.get('/', function (req, res) {
  res.send('Olá FATEC!');
});

app.get('/student', function (req, res) {
  const student: IStudent = {
    name: "Fulano",
    age: 30,
    school: "FATEC"
  }
  res.json(student);
});

app.listen(3000, function () {
  console.log('App está escutando na porta 3000!');
});
